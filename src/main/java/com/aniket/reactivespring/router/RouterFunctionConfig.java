package com.aniket.reactivespring.router;

import com.aniket.reactivespring.handler.SampleHandlerFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

@Configuration
public class RouterFunctionConfig {

    @Bean //Router function
    public RouterFunction<ServerResponse> route(SampleHandlerFunction sampleHandlerFunction)
    {
        return RouterFunctions
                .route(
                        GET("/functional/endpoint1").and(accept(MediaType.APPLICATION_JSON)), //URL Mappting
                        sampleHandlerFunction ::getResponseFlux //Handler for router
                )
                .andRoute(
                        GET("/functional/endpoint2").and(accept(MediaType.APPLICATION_JSON)), //URL Mappting
                        sampleHandlerFunction ::getResponseMono //Handler for router
                )
                ;
    }
}
