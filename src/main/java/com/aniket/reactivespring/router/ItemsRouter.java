package com.aniket.reactivespring.router;


import com.aniket.reactivespring.constants.RestEndpoints;
import com.aniket.reactivespring.handler.ItemsHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;


import static com.aniket.reactivespring.constants.RestEndpoints.V_1_ITEMS;
import static com.aniket.reactivespring.constants.RestEndpoints.V_1_STREAM_FUNCTIONAL_WEB_ITEMS;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;

@Configuration
public class ItemsRouter {

    @Bean
    public RouterFunction<ServerResponse> itemsRoute(ItemsHandler itemsHandler){

        return RouterFunctions
                .route(GET(V_1_ITEMS).and(accept(APPLICATION_JSON))
                ,itemsHandler::getAllItems)
                .andRoute(GET(V_1_ITEMS+"/{id}").and(accept(APPLICATION_JSON))
                ,itemsHandler::getOneItem)
                .andRoute(POST(V_1_ITEMS).and(accept(APPLICATION_JSON))
                ,itemsHandler::createItem)
                .andRoute(DELETE(V_1_ITEMS+"/{id}").and(accept(APPLICATION_JSON))
                        ,itemsHandler::deleteItem)
                .andRoute(PUT(V_1_ITEMS+"/{id}").and(accept(APPLICATION_JSON))
                        ,itemsHandler::updateItem);
    }

    @Bean
    public RouterFunction<ServerResponse> errorRoute(ItemsHandler itemsHandler){
        return RouterFunctions
                .route(GET("/fun/runtimeexception").and(accept(APPLICATION_JSON))
                        ,itemsHandler::itemsEx);

    }

    @Bean
    public RouterFunction<ServerResponse> itemStreamRoute(ItemsHandler itemsHandler){

        return RouterFunctions
                .route(GET(V_1_STREAM_FUNCTIONAL_WEB_ITEMS).and(accept(APPLICATION_JSON))
                        ,itemsHandler::itemsStream);

    }

}
