package com.aniket.reactivespring.document;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document //-Equivalent to @Entity in RDBMS
@Data
@AllArgsConstructor
public class ItemCapped {

    @Id
    private String id;
    private  String description;
    private Double price;
    private String name;

}
