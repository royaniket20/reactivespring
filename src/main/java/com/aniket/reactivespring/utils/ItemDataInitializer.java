package com.aniket.reactivespring.utils;

import com.aniket.reactivespring.document.Item;
import com.aniket.reactivespring.document.ItemCapped;
import com.aniket.reactivespring.repository.ItemReactiveCappedRepository;
import com.aniket.reactivespring.repository.ReactiveItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

@Component
@Profile("!test")
@Slf4j
public class ItemDataInitializer implements CommandLineRunner {
    
    @Autowired
    private ReactiveItemRepository reactiveItemRepository;


    @Autowired
    MongoOperations mongoOperations;

    @Autowired
    private ItemReactiveCappedRepository itemReactiveCappedRepository;

    @Override
    public void run(String... args) throws Exception {
        initialDataSetup();
        createCappedCollection();
        dataSetUpforCappedCollection();
    }

    private void createCappedCollection() {

        //Never use Capped collection for permanenet storage as it is having fixed size
        mongoOperations.dropCollection(ItemCapped.class);
        mongoOperations.createCollection(ItemCapped.class, CollectionOptions.empty().capped().maxDocuments(20).size(50000));

    }

    public void dataSetUpforCappedCollection(){

        Flux<ItemCapped> itemCappedFlux = Flux.interval(Duration.ofSeconds(1))
                .map(i -> new ItemCapped(null,"Random Item " + i, (100.00+i),"Aniket"));

        itemReactiveCappedRepository
                .insert(itemCappedFlux)
                .subscribe((itemCapped -> {
                    log.info("Inserted Item is " + itemCapped);
                }));

    }

    private void initialDataSetup() {

        List<Item> itemList = Arrays.asList(
   new Item(null,"Item1",101.45),
                new Item(null,"Item2",105.23),
                new Item("ABC","Item3",106.23),
                new Item(null,"Item4",144.22),
                new Item("DEF","Item5",477.23),
                new Item(null,"Item6",888.36),
                new Item(null,"Item7",779.23)
        );
        reactiveItemRepository.deleteAll().log() // Remove everything
                .thenMany(Flux.fromIterable(itemList))
                .flatMap(reactiveItemRepository::save)
                .thenMany(reactiveItemRepository.findAll()).subscribe(System.out::println);

            
        }

}
