package com.aniket.reactivespring.controller;



import com.aniket.reactivespring.constants.RestEndpoints;
import com.aniket.reactivespring.document.ItemCapped;
import com.aniket.reactivespring.repository.ItemReactiveCappedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;


@RestController
public class ItemStreamController {

    @Autowired
    ItemReactiveCappedRepository itemReactiveCappedRepository;

    @GetMapping(value = RestEndpoints.V_1_STREAM_ITEMS, produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<ItemCapped> getItemsStream(){

        return itemReactiveCappedRepository.findItemsBy();
    }




}
