package com.aniket.reactivespring.controller;


import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
public class FluxAndMonoController {

    /**
     * This is just a blocking call when we hit from browser as it is a blocking call
     * @return
     */
    @GetMapping("/helloFlux") //Router function
    public Flux<Integer> returnFlux()
    {
        return  Flux.just(1,2,3,4,5,6,7,8,9).delayElements(Duration.ofMillis(100)).log();//Handler Function
    }

    //This will stream data as available
    @GetMapping(value = "/helloFluxStream" ,produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Integer> returnFluxStream()
    {
        return  Flux.just(1,2,3,4,5,6,7,8,9).delayElements(Duration.ofMillis(100)).log();
    }

    //This will stream data as available
    @GetMapping(value = "/helloFluxInfinite" ,produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Integer> returnFluxInfinite()
    {
        return  Flux.interval (Duration.ofMillis(100))
               // .delayElements(Duration.ofMillis(500)) //If there is a downstream operation better to introduce delay so that it can process the next element when it comes
                .map(data->new Integer(data.intValue()))
                .log();
    }

    @GetMapping(value = "/mono")
    public Mono<Integer> returnMono()
    {
        return  Mono.just (100).log();
    }
}
