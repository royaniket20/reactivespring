package com.aniket.reactivespring.controller;

import ch.qos.logback.classic.Logger;
import com.aniket.reactivespring.constants.RestEndpoints;
import com.aniket.reactivespring.document.Item;
import com.aniket.reactivespring.repository.ReactiveItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.aniket.reactivespring.constants.RestEndpoints.V_1_ITEMS;

@RestController
@Slf4j
public class ItemController {

    @Autowired
    private ReactiveItemRepository reactiveItemRepository;

      /* @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleRuntimeException(RuntimeException ex){
        log.error("Exception caught in handleRuntimeException :  {} " , ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
    }*/


    @GetMapping(V_1_ITEMS+"/runtimeException")
    public Flux<Item> runtimeException(){

        return reactiveItemRepository.findAll()
                .concatWith(Mono.error(new RuntimeException("RuntimeException Occurred.")));
    }


    @GetMapping(V_1_ITEMS)
    public Flux<Item> findAllItems()
    {
        return reactiveItemRepository.findAll();
    }

    @GetMapping(V_1_ITEMS+"/{id}") //We are using ResponseEntity instead of Item mono becuase then we can send Response 404 , 200 etc
    //As required
    public Mono<ResponseEntity<Item>> findOneItem(@PathVariable("id") String id)
    {
            return reactiveItemRepository.findById(id).map(item->new ResponseEntity<>(item, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }



    @PostMapping(V_1_ITEMS)
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Item> createOneItem(@RequestBody Item item)
    {
        return reactiveItemRepository.save(item);
    }

    @DeleteMapping(V_1_ITEMS+"/{id}")
    public Mono<Void> deleteOneItem(@PathVariable("id") String id)
    {
        return reactiveItemRepository.deleteById(id);
    }

    @PutMapping(V_1_ITEMS+"/{id}")
    public Mono<ResponseEntity<Item>> updateOneItem(@RequestBody Item itemtoUpdate,@PathVariable("id") String id)
    {
        return  reactiveItemRepository.findById(id).flatMap(item->{
             item.setDescription(itemtoUpdate.getDescription());
             item.setPrice(itemtoUpdate.getPrice());
            final Mono<Item> save = reactiveItemRepository.save(item);
            log.info("-------{}",save);
            return save;
         }).map(item->new ResponseEntity<>(item, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
