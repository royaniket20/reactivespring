package com.aniket.reactivespring.constants;

public class RestEndpoints {
    public static final String V_1_ITEMS = "/v1/item";
    public static final String V_1_STREAM_ITEMS = "/v1/stream/item";
    public static final String V_1_STREAM_FUNCTIONAL_WEB_ITEMS = "/v1/stream-web/item";

}
