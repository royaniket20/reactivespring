package com.aniket.reactivespring.repository;

import com.aniket.reactivespring.document.Item;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@DataMongoTest // Will load only necessary classes and use embedded mongo DB
@DirtiesContext //When we add it - each test cases will get new context so during the test case run if context change it will not affect other test cases
public class ReactiveItemRepositoryTest {

    @Autowired
    private ReactiveItemRepository reactiveItemRepository;

    @Before
    public void setup()
    {
        List<Item> itemList = Arrays.asList(
                new Item(null,"Apple Watch",45.65),
                new Item(null,"Bettel Watch",55.26),
                new Item("MYID","Bose Watch",55.26),
                new Item(null,"Nokia Watch",47.23));
        reactiveItemRepository.deleteAll();
        reactiveItemRepository.saveAll(itemList).blockLast(); //This will block the call till the all items stored in DB


    }

    @Test
    public void getAllItemsTest()
    {
        final Flux<Item> allItems = reactiveItemRepository.findAll();
        StepVerifier.create(allItems.log())
                .expectSubscription()
                .expectNextCount(4)
                .verifyComplete();//As now the Data is not there
    }

    @Test
    public void saveItemsTest()
    {
        //ID will be auto generated
        Item item = new Item(null,"Playstation 5",499.77);
        final Mono<Item> savedItem = reactiveItemRepository.save(item);
        StepVerifier.create(savedItem.log("Log-prefix : ")) //Good for tracking purpose
                .expectSubscription()
                .expectNextCount(1)
                .verifyComplete();//As now the Data is not there
    }

    @Test
    public void getItemByIdTest()
    {
        final Mono<Item> uniqueItem = reactiveItemRepository.findById("MYID");
        StepVerifier.create(uniqueItem.log())
                .expectSubscription()
                .expectNextMatches(item->item.getDescription().equals("Bose Watch"))
                .verifyComplete();//As now the Data is not there
    }

    @Test
    public void getItemByDescriptionTest()
    {
        final Flux<Item> fluxItems = reactiveItemRepository.findByDescription("Apple Watch");
        StepVerifier.create(fluxItems.log())
                .expectSubscription()
                .expectNextMatches(item->item.getPrice()==45.65)
                .verifyComplete();//As now the Data is not there
    }

    @Test
    public void updateItemTest()
    {
        Double newPrice = 788.65;
        final Flux<Item> fluxItems = reactiveItemRepository.findByDescription("Apple Watch");
        final Flux<Item> updatedItem =  fluxItems.map(item->{
            item.setPrice(newPrice);
            return item;
        }).flatMap(item-> {
            return reactiveItemRepository.save(item);
        });



        StepVerifier.create(updatedItem.log())
                .expectSubscription()
                .expectNextMatches(item->item.getPrice()==newPrice)
                .verifyComplete();//As now the Data is not there
    }

    @Test
    public void deleteItemTest()
    {
        final Flux<Item> fluxItems = reactiveItemRepository.findByDescription("Apple Watch");
        final Flux<Void> deletedItem =  fluxItems.map(item->{
            return item.getId();
        }).flatMap(itemId-> {
            return reactiveItemRepository.deleteById(itemId);
        });



        StepVerifier.create(deletedItem.log())
                .expectSubscription()
                .verifyComplete();//As now the Data is not there


        final Flux<Item> remainingFluxItems = reactiveItemRepository.findAll();

        StepVerifier.create(remainingFluxItems.log())
                .expectSubscription()
                .expectNextCount(3)
                .verifyComplete();
    }

}
