package com.aniket.reactivespring.handler;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;


//@WebFluxTest
@SpringBootTest //As webFluxTest annotation does not scan components
@AutoConfigureWebTestClient //To create WebTestClient bean
@DirtiesContext
public class SampleHandlerFunctionTest {

    @Autowired
    private WebTestClient webTestClient;


    @Test
    public void functionWebMonoTest()
    {
        Integer expected =new Integer(1);
      webTestClient.get()
                .uri("/functional/endpoint2")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Integer.class)
                .consumeWith((response)->{
                    Assert.assertEquals(expected,response.getResponseBody());
                });

    }


    @Test
    public void functionWebFluxTest()
    {
        final Flux<Integer> responseBody = webTestClient.get()
                .uri("/functional/endpoint1")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .returnResult(Integer.class)
                .getResponseBody();

        StepVerifier.create(responseBody.log())
                .expectSubscription()//Optional
                .expectNext(1,2,3,4,5,6)
                .verifyComplete();
    }

}
