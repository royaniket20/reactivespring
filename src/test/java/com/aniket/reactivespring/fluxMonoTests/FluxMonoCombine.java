package com.aniket.reactivespring.fluxMonoTests;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;

public class FluxMonoCombine {

    @Test
    public void combineUsingMerge()
    {
        Flux<String> flux1 = Flux.just("A","B","C");
        Flux<String> flux2 = Flux.just("D","E","F");
        //Flux merging does not mean that it will emit to subscribe sequentially
        //If one of the flux is blocking other flux will start emitting
        Flux<String> merged = Flux.merge(flux1,flux2).log();

        StepVerifier.create(merged).expectNextCount(6).verifyComplete(); //Emit - A,B,C,D,E,F


    }

    @Test
    public void combineUsingMergeDeplay()
    {
        Flux<String> flux1 = Flux.just("A","B","C").delayElements(Duration.ofSeconds(1));
        Flux<String> flux2 = Flux.just("D","E","F").delayElements(Duration.ofSeconds(1));
        //Flux merging does not mean that it will emit to subscribe sequentially
        //If one of the flux is blocking other flux will start emitting
        Flux<String> merged = Flux.merge(flux1,flux2).log();

        StepVerifier.create(merged).expectNext("A","D","B","E","C","F").verifyComplete(); //Emit - A,D,B,E,C,F

    }

    @Test
    public void combineUsingMergeDeplayConcat()
    {
        Flux<String> flux1 = Flux.just("A","B","C").delayElements(Duration.ofSeconds(1));
        Flux<String> flux2 = Flux.just("D","E","F").delayElements(Duration.ofSeconds(1));
        //Flux merging does not mean that it will emit to subscribe sequentially - You can use concat
        //If one of the flux is blocking other flux will start emitting
        Flux<String> merged = Flux.concat(flux1,flux2).log();

        StepVerifier.create(merged).expectNext("A","B","C","D","E","F").verifyComplete(); //Emit - A,B,C,D,E,F


    }

    @Test
    public void combineUsingMergeZip()
    {
        Flux<String> flux1 = Flux.just("A","B","C").delayElements(Duration.ofSeconds(1));
        Flux<String> flux2 = Flux.just("D","E","F").delayElements(Duration.ofSeconds(1));

        Flux<String> merged = Flux.zip(flux1,flux2,(t1,t2) ->{
            return  t1.concat(t2);
        } ).log();

        StepVerifier.create(merged).expectNext("AD","BE","CF").verifyComplete();
    }
}
