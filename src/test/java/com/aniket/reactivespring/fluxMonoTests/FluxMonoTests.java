package com.aniket.reactivespring.fluxMonoTests;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class FluxMonoTests {

    @Test
    public void fluxTest()
    {
        Flux<String> fluxString = Flux.just("Aniket","Rimi","Sriparna")
      /* .concatWith(Flux.error(new RuntimeException("Custom Error")))*/
                .concatWith(Flux.just("After error")) //It will execute of exception does not occure
        .log();
        //Printing and handling exception
        fluxString.subscribe(System.out::println,(e)->System.err.println(e.getMessage()),()->System.out.println("If flux successfully completed"));
    }

    @Test
    public void fluxTestElements_WithoutError()
    {
        Flux<String> fluxString = Flux.just("Aniket","Rimi","Sriparna")
                .log();
// StepVerifier will automatically subscribe to flux
        //verifyComplete or verifyError need to be called which will actually start the data flow
        StepVerifier.create(fluxString)
                .expectNext("Aniket")
                .expectNext("Rimi")
                .expectNext("Sriparna")
               .verifyComplete();
    }

    @Test
    public void fluxTestElements_WithError()
    {
        Flux<String> fluxString = Flux.just("Aniket","Rimi","Sriparna")
                .concatWith(Flux.error(new RuntimeException("Custom Error")))
                .log();
// StepVerifier will automatically subscribe to flux
        //verifyComplete or verify need to be called which will actually start the data flow
        StepVerifier.create(fluxString)
//                .expectNext("Aniket")
//                .expectNext("Rimi")
//                .expectNext("Sriparna")
                 .expectNext("Aniket","Rimi","Sriparna")
                //Eithe one of them can be called
                .expectError(RuntimeException.class)
                //.expectErrorMessage("Custom Error")
                .verify();
    }

    @Test
    public void fluxTestElements_Count()
    {
        Flux<String> fluxString = Flux.just("Aniket","Rimi","Sriparna")
                .concatWith(Flux.error(new RuntimeException("Custom Error")))
                .log();
// StepVerifier will automatically subscribe to flux
        //verifyComplete or verify OR verifyError need to be called which will actually start the data flow
        StepVerifier.create(fluxString)
               .expectNextCount(3)
                .verifyError();
    }


    @Test
    public void monoTestElements_Basic()
    {
        Mono<String> monoString = Mono.just("Aniket")
                .log();
// StepVerifier will automatically subscribe to flux
        //verifyComplete or verify OR verifyError need to be called which will actually start the data flow
        StepVerifier.create(monoString)
                //only one of them can be called
               // .expectNextCount(1)
                .expectNext("Aniket")
                .verifyComplete();
    }

    @Test
    public void monoTestElements_WithError()
    {
// StepVerifier will automatically subscribe to mono
        //verifyComplete or verify need to be called which will actually start the data flow
        StepVerifier.create(Mono.error(new RuntimeException("Custom Error")).log())
                //Eithe one of them can be called
                //.expectError(RuntimeException.class)
                .expectErrorMessage("Custom Error")
                .verify();
    }

}
