package com.aniket.reactivespring.fluxMonoTests;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;

public class FluxAndMonoTimeTest {

    @Test
    public void infiniteFluxBasics() throws InterruptedException {
        Flux<Long> intiniteSerise = Flux.interval(Duration.ofMillis(200));
        intiniteSerise.subscribe(k-> System.out.println("Value : "+k)); //This will immediately return

        Thread.sleep(3000); //This is given to keep main method alive to see the outputs of onReceive


    }

    @Test
    public void infiniteFluxTest() throws InterruptedException {
        Flux<Integer> intiniteSerise = Flux.interval(Duration.ofMillis(200))
                .delayElements(Duration.ofSeconds(1))//Put additional delay to element emitting after 100 ms mentioned above
                .take(5)//Take first 5 elements
                .map(k->new Integer(k.intValue()))
                .log();

        StepVerifier.create(intiniteSerise).expectSubscription()
                .expectNext(0,1,2,3,4)
                .verifyComplete();


    }


}
