package com.aniket.reactivespring.fluxMonoTests;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FluxAndMonoErrorTest {

    List<String> names = Arrays.asList("Aniket","Rimi","Sriparna","Tapan");
    @Test
    public void fluxErrorHandling()
    {
        Flux<String> flux = Flux.fromIterable(names)
                .concatWith(Flux.error(new RuntimeException("Custom Error")))
                .concatWith(Flux.just("Others"))
                .log();
        StepVerifier.create(flux).expectNextCount(4)
                .expectError(RuntimeException.class)
                .verify();

        //Now lets handle error
        //Even if Exception is handled still after that whatever element is there it will not be emitted ever - like 'Others'
        flux = Flux.fromIterable(names)
                .concatWith(Flux.error(new RuntimeException("Custom Error")))
                .concatWith(Flux.just("Others"))
               /* .onErrorResume(e->{
                    System.out.println(e.getMessage());
                    return  Flux.just("Exception occurred");
                })*/
                .onErrorReturn("Exception occurred")//Single fallback value
                .log();
        StepVerifier.create(flux).expectNextCount(5).verifyComplete();

        flux = Flux.fromIterable(names)
                .concatWith(Flux.error(new RuntimeException("Custom Error")))
                .concatWith(Flux.just("Others"))
                .onErrorMap(k->new IOException(k)) //Mapping one Exception ot another type of Exception
               // .retry(2) //How may time you want to retry flux from beginning
                .retryBackoff(2, Duration.ofSeconds(3)) //How may time you want to retry flux from beginning after some interval of backoff
                .log();
        StepVerifier.create(flux)
                .expectNext(names.toArray(new String[names.size()]))//actual
                .expectNext(names.toArray(new String[names.size()]))//retry 1
                .expectNext(names.toArray(new String[names.size()]))//retry 2
               // .expectError(IOException.class)
                .expectError(IllegalStateException.class) //In case you use retry backoff this exception occure whern retry exhausted
                .verify();
    }



}
