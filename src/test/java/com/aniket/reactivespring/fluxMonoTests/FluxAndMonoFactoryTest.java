package com.aniket.reactivespring.fluxMonoTests;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class FluxAndMonoFactoryTest {

    List<String> names = Arrays.asList("Aniket","Rimi","Sriparna","Tapan");
    @Test
    public void fluxUsingIterable()
    {
        Flux<String> flux = Flux.fromIterable(names).log();
        StepVerifier.create(flux).expectNextCount(4).verifyComplete();
    }

    @Test
    public void fluxUsingArray()
    {
        String[] nameArr = names.stream().toArray(String[]::new);
        Flux<String> flux = Flux.fromArray(nameArr).log();
        StepVerifier.create(flux).expectNextCount(4).verifyComplete();
    }


    @Test
    public void fluxUsingStream()
    {
       Flux<String> flux = Flux.fromStream (names.stream()).log();
        StepVerifier.create(flux).expectNextCount(4).verifyComplete();
    }

    @Test
    public void monoUsingJustOrEmpty()
    {
        Mono<Object> mono = Mono.justOrEmpty(null).log();
        StepVerifier.create(mono).expectNextCount(0).verifyComplete();
    }
    @Test
    public void monoUsingSupplier()
    {
        Supplier<String> supplier = ()->"Aniket";
        Mono<String> mono = Mono.fromSupplier(supplier).log();
        StepVerifier.create(mono).expectNextCount(1).verifyComplete();
    }

    @Test
    public void fluxUsingRange()
    {
        Flux<Integer> flux = Flux.range (1,8).log();
        StepVerifier.create(flux).expectNext(1,2,3,4,5,6,7,8).verifyComplete();
    }
}
