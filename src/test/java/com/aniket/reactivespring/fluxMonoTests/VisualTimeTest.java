package com.aniket.reactivespring.fluxMonoTests;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;
import reactor.test.scheduler.VirtualTimeScheduler;

import java.time.Duration;

public class VisualTimeTest {

    @Test
    public void testWithoutVisualTime() throws InterruptedException {
        Flux<String> stringFlux = Flux.just("A","B","C","D","E","F")
                .delayElements(Duration.ofMillis(500));

        StepVerifier.create(stringFlux.log()).expectNext("A","B","C","D","E","F").verifyComplete();
   //To check the time we are using machine clock

    }

    @Test
    public void testWithVisualTime() throws InterruptedException {
        VirtualTimeScheduler.getOrSet(); //Enable virtual clock
        Flux<String> stringFlux = Flux.just("A","B","C","D","E","F")
                .delayElements(Duration.ofMillis(500)); //This will have no effect if we are using Virtual Time Scheduler as now machine clock does not matter
        StepVerifier.withVirtualTime(()->stringFlux.log())
                .thenAwait(Duration.ofSeconds(3))//This is required when using virtual timer - here input is 3 sec = 500*6 ms
                .expectNext("A","B","C","D","E","F")
                .verifyComplete();//This will execute immediately
    }
}
