package com.aniket.reactivespring.fluxMonoTests;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;

public class FluxBackPressure {

    @Test
    public void fluxBackPressureTest() throws InterruptedException {
        Flux<Integer> integerFlux = Flux.range(0,200)
                .log();


        //Programmatically take control on back pressure
        integerFlux.subscribe(k-> System.out.println("Element is : "+k)
                ,(e)-> System.out.println("Error is : "+e.getMessage())
        ,()-> System.out.println("Flux completed successfully...,") //This will not get executed because we are not consuming the whole flux
        ,(subscription)->{
            subscription.request(2);
                });

        StepVerifier.create(integerFlux)
                .expectSubscription()
                .thenRequest(2)
                .thenRequest(20)
                .thenRequest(40)
                .thenCancel()
                .verify();


        //Flux back pressure customized back pressure
        integerFlux.subscribe(new BaseSubscriber<Integer>() {
            @Override
            protected void hookOnNext(Integer value) {
              request(1);
                System.out.println("Element is : "+value);
              if(value==7)
              {
                  cancel();
              }
            }
        });


    }
}
