package com.aniket.reactivespring.fluxMonoTests;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class ColdHotPublisher {

    @Test
    public void coldPublisherTest() throws InterruptedException {
        Flux<String> stringFlux = Flux.just("A","B","C","D","E","F")
                .delayElements(Duration.ofMillis(500));
        stringFlux.subscribe(s-> System.out.println("Value 1 : "+s)); //Every time you subscribe it will start emitting from beginning - A,B,C,D,E,F
        Thread.sleep(1500);
        stringFlux.subscribe(s-> System.out.println("Value 2 : "+s)); //Every time you subscribe it will start emitting from beginning - This is a cold publisher
        Thread.sleep(3000);
    }

    @Test
    public void hotPublisherTest() throws InterruptedException {
        Flux<String> stringFlux = Flux.just("A","B","C","D","E","F")
                .delayElements(Duration.ofMillis(500));
        ConnectableFlux<String> hotFlux = stringFlux.publish();
        hotFlux.connect();
        hotFlux.subscribe(s-> System.out.println("Value 1 : "+s)); //Now this is a hot flux
        Thread.sleep(1500);
        hotFlux.subscribe(s-> System.out.println("Value 2 : "+s)); //Now this is a hot flux
        Thread.sleep(3000);
    }
}
