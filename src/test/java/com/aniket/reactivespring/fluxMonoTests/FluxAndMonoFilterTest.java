package com.aniket.reactivespring.fluxMonoTests;

import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import org.springframework.util.StopWatch;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import static reactor.core.scheduler.Schedulers.parallel;

public class FluxAndMonoFilterTest {

    List<String> names = Arrays.asList("Aniket","Rimi","Sriparna","Tapan");
    @Test
    public void fluxUsingIterableFilter()
    {
        Flux<String> flux = Flux.fromIterable(names)
                .filter(name-> (name.startsWith("A") || name.startsWith("S")));
        StepVerifier.create(flux.log()).expectNextCount(2).verifyComplete();
    }

    @Test
    public void fluxUsingIterableTransform()
    {
        Flux<String> flux = Flux.fromIterable(names)
                .map(name-> name.toUpperCase());
        StepVerifier.create(flux.log()).expectNext("ANIKET","RIMI","SRIPARNA","TAPAN").verifyComplete();
    }

    @Test
    public void fluxUsingIterableTransformLength()
    {
        Flux<Integer> flux = Flux.fromIterable(names)
                .map(name-> name.length())
                .repeat(1); // repeate same flux n nummber of time
        StepVerifier.create(flux.log()).expectNext(6,4,8,5).expectNext(6,4,8,5).verifyComplete();
    }


    @Test
    public void fluxUsingIterableTransformFilter()
    {
        Flux<String> flux = Flux.fromIterable(names)
                .map(name-> name.toUpperCase())
                .filter(name -> name.length()>=5);
        StepVerifier.create(flux.log()).expectNext("ANIKET","SRIPARNA","TAPAN").verifyComplete();
    }

    @Test
        public void fluxUsingIterableTransformFlatMap()
    {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Flux<String> flux = Flux.fromIterable(names)
                .flatMap(name-> Flux.fromIterable(convertToFluxList(name))).log(); //a->a , AdditionalValue_a  for each ad every element =Flux<String> return from this line
        StepVerifier.create(flux).expectNextCount(8).verifyComplete();
        stopWatch.stop();
        System.out.println("Time taken - "+stopWatch.getLastTaskTimeMillis());
    }

    private List<String> convertToFluxList(String name) {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return  Arrays.asList(name,"AdditionalValue_"+name);
    }

    @Test
    public void fluxUsingIterableTransformFlatMapParallel()
    {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Flux<String> flux = Flux.fromIterable(names)
                .window(2)  // pair two elements and send it give Flux<Flux<String>> of 2 elements
                .flatMap(name-> name.map(k->convertToFluxList(k)) //Flux<List<String>> return from this line
                        .subscribeOn(parallel())) // parallel
                .flatMap(s->Flux.fromIterable(s)); //Flux<String> return from this line

        StepVerifier.create(flux.log()).expectNextCount(8).verifyComplete();
        stopWatch.stop();
        System.out.println("Time taken - "+stopWatch.getLastTaskTimeMillis());
    }

    @Test
    public void fluxUsingIterableTransformFlatMapParallel_MaintainOrder()
    {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Flux<String> flux = Flux.fromIterable(names)
                .window(2)  // pair two elements and send it give Flux<Flux<String>>
              /*  .flatMap(name-> name.map(k->convertToFluxList(k)) //Flux<List<String>>
                        .subscribeOn(parallel())) // parallel*/
                //concatMap - will maintain order but will make call sequential again
               /* .concatMap(name-> name.map(k->convertToFluxList(k)) //Flux<List<String>>
                        .subscribeOn(parallel())) // parallel*/
                //flatMapSequential - It will do parallel and maintain order
                .flatMapSequential(name-> name.map(k->convertToFluxList(k)) //Flux<List<String>>
                        .subscribeOn(parallel())) // parallel
                .flatMap(s->Flux.fromIterable(s)); //Flux<String>

        StepVerifier.create(flux.log()).expectNextCount(8).verifyComplete();
        stopWatch.stop();
        System.out.println("Time taken - "+stopWatch.getLastTaskTimeMillis());
    }


}
