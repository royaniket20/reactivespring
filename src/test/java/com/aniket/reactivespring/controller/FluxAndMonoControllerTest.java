package com.aniket.reactivespring.controller;


import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;


//@WebFluxTest //For flux testing only for controller -But it will not scan @component @Repository etc
//@DirtiesContext

@SpringBootTest
@DirtiesContext
@Slf4j
@AutoConfigureWebTestClient
public class FluxAndMonoControllerTest {

    @Autowired
     WebTestClient webTestClient; //@WebFluxTest  this will create instance of this class

   @Test
    public void returnFluxTest()
    {
        final Flux<Integer> responseBody = webTestClient.get()
                .uri("/helloFlux")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .returnResult(Integer.class)
                .getResponseBody();

        StepVerifier.create(responseBody.log())
                .expectSubscription()//Optional
                .expectNext(1,2,3,4,5,6,7,8,9)
                .verifyComplete();
    }

    @Test
    public void returnFluxStream()
    {
        webTestClient.get()
                .uri("/helloFlux")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Integer.class)
                .hasSize(9);

        final EntityExchangeResult<List<Integer>> listEntityExchangeResult = webTestClient.get()
                .uri("/helloFlux")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Integer.class)
                .hasSize(9)
                .returnResult();
List<Integer> expected = Arrays.asList(1,2,3,4,5,6,7,8,9);
        Assert.assertEquals(expected,listEntityExchangeResult.getResponseBody());


          webTestClient.get()
                .uri("/helloFlux")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Integer.class)
                .consumeWith((response)->{
                    Assert.assertEquals(expected,response.getResponseBody());
                });

    }


    @Test
    public void returnFluxInfiniteStream()
    {
        final Flux<Integer> responseBody = webTestClient.get()
                .uri("/helloFluxInfinite")
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .exchange()
                .expectStatus().isOk()
                .returnResult(Integer.class)
                .getResponseBody();

          StepVerifier.create(responseBody.log())
                  .expectSubscription()
                  .expectNext(0,1,2,3,4)
                  .thenCancel()
                  .verify();

    }

    @Test
    public void returnMonoTest()
    {
     Integer expected =new Integer(100);
        webTestClient.get()
                .uri("/mono")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Integer.class)
                .consumeWith((response)->{
                    Assert.assertEquals(expected,response.getResponseBody());
                });


    }

}
