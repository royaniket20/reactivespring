package com.aniket.reactivespring.controller;

import com.aniket.reactivespring.constants.RestEndpoints;
import com.aniket.reactivespring.document.Item;
import com.aniket.reactivespring.repository.ReactiveItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;

import static com.aniket.reactivespring.constants.RestEndpoints.V_1_ITEMS;

@SpringBootTest
@RunWith(SpringRunner.class)
@DirtiesContext
@Slf4j
@AutoConfigureWebTestClient
public class ItemControllerTest {

    @Autowired
    private ReactiveItemRepository reactiveItemRepository;

    @Autowired
    private WebTestClient webTestClient;

    @Before
    public void initialDataSetup() {

        List<Item> itemList = Arrays.asList(
                new Item(null,"Item1",101.45),
                new Item(null,"Item2",105.23),
                new Item("ABC","Item3",106.23),
                new Item(null,"Item4",144.22),
                new Item("DEF","Item5",477.23),
                new Item(null,"Item6",888.36),
                new Item(null,"Item7",779.23)
        );
        reactiveItemRepository.deleteAll().log() // Remove everything
                .thenMany(Flux.fromIterable(itemList))
                .flatMap(reactiveItemRepository::save).blockLast();
     log.info("Data setup is now completed");
                (reactiveItemRepository.findAll()).subscribe(System.out::println);


    }


@Test
    public void findAllItems_Test()
{
    webTestClient.get().uri(V_1_ITEMS).exchange()
            .expectStatus().isOk()
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBodyList(Item.class)
            .hasSize(7);
}


    @Test
    public void findAllItems_Test_Advance()
    {
        webTestClient.get().uri(V_1_ITEMS).exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Item.class)
                .hasSize(7).consumeWith(item->{
                  List<Item> itemList = item.getResponseBody();
                  itemList.forEach(it->{
                      Assert.assertTrue(it.getId()!=null);
                  });
        });
    }


    @Test
    public void findAllItems_Test_Advance2()
    {
        final Flux<Item> responseBody = webTestClient.get().uri(V_1_ITEMS).exchange()
                .returnResult(Item.class).getResponseBody();

        StepVerifier.create(responseBody.log("Result fom Flux : "))
                .expectSubscription().expectNextCount(7).verifyComplete();
    }


   @Test
    public void findOneItem_Basic()
    {
        webTestClient.get().uri(V_1_ITEMS.concat("/{id}"),"ABC")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.price").isEqualTo(106.23);


    }

    @Test
    public void findOneItem_Advance()
    {
        webTestClient.get().uri(V_1_ITEMS.concat("/{id}"),"XYZ")
                .exchange()
                .expectStatus().isNotFound();



    }



    @Test
    public void createOneItem()
    {
        webTestClient.post().uri(V_1_ITEMS)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(new Item(null,"MY Phone",999.99)),Item.class)
                .exchange()//Actual connect happens here
                .expectStatus().isCreated()
                .expectBody()
                .jsonPath("$.price").isEqualTo(999.99)
                .jsonPath("$.description").isEqualTo("MY Phone")
                .jsonPath("$.id").isNotEmpty();
        ;



    }

    @Test
    public void deleteOneItem()
    {
        webTestClient.delete().uri(V_1_ITEMS.concat("/{id}"),"ABC")
                .exchange();
        webTestClient.get().uri(V_1_ITEMS).exchange().expectBodyList(Item.class)
        .hasSize(6);


    }


    @Test
    public void updateOneItem()
    {
        log.info("Updating item --------");
        Item item = new Item(null,"Updated Item",888.88);
        webTestClient.put().uri(V_1_ITEMS.concat("/{id}"),"DEF")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(item),Item.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.price").isEqualTo(888.88);

        final Flux<Item> responseBody =   webTestClient.get().uri(V_1_ITEMS).exchange().returnResult(Item.class).getResponseBody();
         StepVerifier.create(responseBody.log("ELEMENTS AFTER UPDATE")).expectSubscription().expectNextCount(7).verifyComplete();

    }


    @Test
    public void runTimeException(){
        webTestClient.get().uri(V_1_ITEMS+"/runtimeException")
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody(String.class)
                .isEqualTo("RuntimeException Occurred.");



    }



}
