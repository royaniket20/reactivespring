package com.aniket.reactivespring;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext
class ReactiveSpringApplicationTests {

	@Test
	void contextLoads() {
	}

}
